package cn.jpanda.elastic.job.autoconfigure.listener;

import com.dangdang.ddframe.job.lite.api.JobScheduler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Map;

/**
 * 作业初始化调用监听器
 * 在Spring容器完成之后，该接口执行作业初始化方法
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/19 15:34
 */
@Slf4j
public class JobSchedulerInitListener implements ApplicationListener<ApplicationReadyEvent> {

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        ConfigurableApplicationContext context = event.getApplicationContext();
        Map<String, JobScheduler> jobs = context.getBeansOfType(JobScheduler.class);
        jobs.forEach((name, job) -> {
            log.info("{} job is started...", name);
        });

    }
}
