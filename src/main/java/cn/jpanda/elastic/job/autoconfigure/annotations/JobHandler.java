package cn.jpanda.elastic.job.autoconfigure.annotations;

import com.dangdang.ddframe.job.executor.handler.ExecutorServiceHandler;
import com.dangdang.ddframe.job.executor.handler.JobExceptionHandler;
import com.dangdang.ddframe.job.executor.handler.impl.DefaultExecutorServiceHandler;
import com.dangdang.ddframe.job.executor.handler.impl.DefaultJobExceptionHandler;

import java.lang.annotation.*;

/**
 * 作业处理器
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/20 10:53
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface JobHandler {
    /**
     * 线程处理器
     */
    Class<? extends ExecutorServiceHandler> executorServiceHandlers() default DefaultExecutorServiceHandler.class;

    /**
     * 异常处理器集合
     */
    Class<? extends JobExceptionHandler> exceptionHandlers() default DefaultJobExceptionHandler.class;
}
