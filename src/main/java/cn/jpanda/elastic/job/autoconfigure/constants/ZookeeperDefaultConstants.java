package cn.jpanda.elastic.job.autoconfigure.constants;

/**
 * Zookeeper配置默认值常量类
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/18 13:59
 */
public interface ZookeeperDefaultConstants {
    /**
     * 等待重试的间隔时间的初始值,单位毫秒
     */
    int baseSleepTimeMilliseconds = 1000;
    /**
     * 等待重试的间隔时间的最大值，单位毫秒
     */
    int maxSleepTimeMilliseconds = 3000;
    /**
     * 最大重试次数.
     */
    int maxRetries = 3;

    /**
     * 会话超时时间.
     * 单位毫秒.
     */
    int sessionTimeoutMilliseconds = 60000;

    /**
     * 连接超时时间.
     * 单位毫秒.
     */
    int connectionTimeoutMilliseconds = 15000;
}
