package cn.jpanda.elastic.job.autoconfigure.exception;

import org.springframework.core.NestedRuntimeException;

/**
 *  ElasticJob 注解解析类冲突异常
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/21 20:57
 */
public class ElasticJobParserConflictRuntimeException extends NestedRuntimeException {
    public ElasticJobParserConflictRuntimeException(String msg) {
        super(msg);
    }
}
