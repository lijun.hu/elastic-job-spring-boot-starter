package cn.jpanda.elastic.job.autoconfigure.annotations.metedata;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * Job注解定义的元注解，该注解用于修饰所有【修饰Job的注解】.
 * 被该元注解标注的注解，将会用来表示ElasticJob作业，该注解集成了{@link Component}的能力
 * ,并会将该能力传递给被注解的注解上，该注解也是定义任务的基本构成。
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/18 9:44
 */
@Inherited
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ElasticJobMeteData {
}
