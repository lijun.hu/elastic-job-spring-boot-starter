package cn.jpanda.elastic.job.autoconfigure.annotations;

import com.dangdang.ddframe.job.event.JobEventConfiguration;
import com.dangdang.ddframe.job.event.rdb.JobEventRdbConfiguration;

import java.lang.annotation.*;

/**
 * 事件追踪
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/7 9:34
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface JobEvent {
    /**
     * 是否启用事件追踪功能
     */
    boolean enabled() default true;

    /**
     * 处理事件追踪的处理类
     */
    Class<? extends JobEventConfiguration> eventHandlerClass() default JobEventRdbConfiguration.class;

}
