package cn.jpanda.elastic.job.autoconfigure.configuration;

import cn.jpanda.elastic.job.autoconfigure.constants.ZookeeperDefaultConstants;
import cn.jpanda.elastic.job.autoconfigure.properties.ZookeeperProperties;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

/**
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/18 13:54
 */
@EnableConfigurationProperties({ZookeeperProperties.class})
public class ZookeeperConfiguration implements InitializingBean {

    @Resource
    ZookeeperProperties zookeeperProperties;

    @Override
    public void afterPropertiesSet() {
        if (ObjectUtils.isEmpty(zookeeperProperties.getBaseSleepTimeMilliseconds())) {
            zookeeperProperties.setBaseSleepTimeMilliseconds(ZookeeperDefaultConstants.baseSleepTimeMilliseconds);
        }
        if (ObjectUtils.isEmpty(zookeeperProperties.getMaxSleepTimeMilliseconds())) {
            zookeeperProperties.setMaxSleepTimeMilliseconds(ZookeeperDefaultConstants.maxSleepTimeMilliseconds);
        }
        if (ObjectUtils.isEmpty(zookeeperProperties.getMaxRetries())) {
            zookeeperProperties.setMaxRetries(ZookeeperDefaultConstants.maxRetries);
        }
        if (ObjectUtils.isEmpty(zookeeperProperties.getSessionTimeoutMilliseconds())) {
            zookeeperProperties.setSessionTimeoutMilliseconds(ZookeeperDefaultConstants.sessionTimeoutMilliseconds);
        }
        if (ObjectUtils.isEmpty(zookeeperProperties.getConnectionTimeoutMilliseconds())) {
            zookeeperProperties.setConnectionTimeoutMilliseconds(ZookeeperDefaultConstants.connectionTimeoutMilliseconds);
        }
    }

}
