package cn.jpanda.elastic.job.autoconfigure.constants;

import cn.jpanda.elastic.job.autoconfigure.configuration.ZookeeperConfiguration;

/**
 * 自动配置常量类
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/18 15:40
 */
public interface ConfigurationConstants {
    /**
     * Zookeeper配置类
     */
    String ZOOKEEPER_PROPERTIES_CONFIGURATION = ZookeeperConfiguration.class.getCanonicalName();
    /**
     * [开启]参数名称
     */
    String ENABLED_ELASTIC_JOB_ENABLED_ATTR_NAME = "enabled";

}
