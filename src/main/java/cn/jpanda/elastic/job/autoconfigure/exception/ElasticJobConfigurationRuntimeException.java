package cn.jpanda.elastic.job.autoconfigure.exception;

import org.springframework.core.NestedRuntimeException;

/**
 * Elastic Job 配置异常
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/18 19:07
 */
public class ElasticJobConfigurationRuntimeException  extends NestedRuntimeException {

    public ElasticJobConfigurationRuntimeException(String msg) {
        super(msg);
    }

    public ElasticJobConfigurationRuntimeException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
