package cn.jpanda.elastic.job.autoconfigure.strategy;

import cn.jpanda.elastic.job.autoconfigure.annotations.JobParser;
import cn.jpanda.elastic.job.autoconfigure.annotations.jobs.ScriptElasticJob;
import com.dangdang.ddframe.job.config.script.ScriptJobConfiguration;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.ScannedGenericBeanDefinition;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

/**
 * 脚本型作业注解解析器
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/25 17:40
 */
@JobParser(ScriptElasticJob.class)
public class ScriptJobParseAnnotationHandlerStrategy extends AbstractJobParseAnnotationHandlerStrategy {
    @Override
    protected AnnotationAttributes getJobCoreAnnotationAttrs(ScannedGenericBeanDefinition jobBeanDefinition, AnnotationMetadata metadata) {
        return AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(ScriptElasticJob.class.getCanonicalName()));
    }

    @Override
    protected BeanDefinition getJobTypeConfigurationBeanDefinition(ScannedGenericBeanDefinition jobBeanDefinition, AnnotationMetadata metadata) {
        BeanDefinitionBuilder result = BeanDefinitionBuilder.rootBeanDefinition(ScriptJobConfiguration.class);
        result.addConstructorArgValue(getJobCoreConfiguration(jobBeanDefinition, metadata));

        AnnotationAttributes
                scriptJobAttributes = AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(ScriptElasticJob.class.getCanonicalName()));
        assert scriptJobAttributes != null;
        result.addConstructorArgValue(scriptJobAttributes.getString("scriptCommandLine"));
        return result.getBeanDefinition();
    }
}
