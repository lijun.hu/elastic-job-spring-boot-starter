package cn.jpanda.elastic.job.autoconfigure.strategy;

import cn.jpanda.elastic.job.autoconfigure.annotations.JobParser;
import cn.jpanda.elastic.job.autoconfigure.annotations.jobs.SimpleElasticJob;
import com.dangdang.ddframe.job.config.simple.SimpleJobConfiguration;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.ScannedGenericBeanDefinition;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

/**
 * SimpleElasticJob 注解解析类
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/21 21:04
 */
@JobParser(SimpleElasticJob.class)
public class SimpleJobParseAnnotationHandlerStrategy extends AbstractJobParseAnnotationHandlerStrategy {
    @Override
    protected AnnotationAttributes getJobCoreAnnotationAttrs(ScannedGenericBeanDefinition jobBeanDefinition, AnnotationMetadata metadata) {
        return AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(SimpleElasticJob.class.getCanonicalName()));
    }

    @Override
    protected BeanDefinition getJobTypeConfigurationBeanDefinition(ScannedGenericBeanDefinition jobBeanDefinition, AnnotationMetadata metadata) {
        BeanDefinitionBuilder result = BeanDefinitionBuilder.rootBeanDefinition(SimpleJobConfiguration.class);
        result.addConstructorArgValue(getJobCoreConfiguration(jobBeanDefinition, metadata));
        result.addConstructorArgValue(jobBeanDefinition.getBeanClassName());
        return result.getBeanDefinition();
    }
}
