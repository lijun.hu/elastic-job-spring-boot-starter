package cn.jpanda.elastic.job.autoconfigure.annotations;

import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;

import java.lang.annotation.*;

/**
 * 使用该注解指定作业使用的注册中心
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/18 17:01
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface RegistryCenter {
    /**
     * 指定作业使用的注册中心
     * 默认使用 {@link ZookeeperRegistryCenter}
     */
    Class<? extends CoordinatorRegistryCenter> registerCenter() default ZookeeperRegistryCenter.class;
}
