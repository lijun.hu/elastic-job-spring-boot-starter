package cn.jpanda.elastic.job.autoconfigure.annotations;

import cn.jpanda.elastic.job.autoconfigure.annotations.metedata.ElasticJobMeteData;
import cn.jpanda.elastic.job.autoconfigure.configuration.ElasticJobImportSelector;
import cn.jpanda.elastic.job.autoconfigure.configuration.ElasticJobScannerRegister;
import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * * 开启Elastic-Job自动配置
 * * <p>
 * * 该注解将会根据{@link #enabled()}的返回值来决定是否会引入{@link ElasticJobImportSelector}配置导入类。
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/18 15:20
 */
@Component
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({ElasticJobImportSelector.class, ElasticJobScannerRegister.class})
public @interface EnableElasticJob {
    /**
     * 是否启用Elastic-Job自动配置
     */
    boolean enabled() default true;

    /**
     * 扫描Elastic Job作业的基础包路径
     */
    String[] basePackages() default {};

    /**
     * 扫描Elastic Job作业的基础类集合
     * 会将集合类所在包路径作为{@link #basePackages}
     */
    Class<?>[] basePackageClasses() default {};

    /**
     * 自动加载Elastic Job作业，根据路径匹配，不在依赖注解
     */
    boolean smartLoad() default false;

    /**
     * 扫描使用的注解，当{@link #smartLoad()}的值为true时，不生效
     */
    Class<? extends Annotation>[] loadAnnotation() default {ElasticJobMeteData.class};
}
