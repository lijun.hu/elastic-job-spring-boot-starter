package cn.jpanda.elastic.job.autoconfigure.annotations;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 标注解析的注解
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/21 11:53
 */
@Component
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface JobParser {
    /**
     * 解析的注解
     */
    Class<? extends Annotation> value();
}
