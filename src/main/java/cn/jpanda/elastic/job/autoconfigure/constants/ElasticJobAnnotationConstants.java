package cn.jpanda.elastic.job.autoconfigure.constants;

import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;

/**
 * Elastic Job 注解属性常量类
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/25 19:37
 */
public interface ElasticJobAnnotationConstants {
    /**
     * EnableElasticJob参数：是否启用Elastic-Job自动配置
     */
    String ENABLE_ELASTIC_JOB_ATTR_ENABLE = "enabled";
    /**
     * EnableElasticJob参数： 扫描Elastic Job作业的基础包路径
     */
    String ENABLE_ELASTIC_JOB_ATTR_BASE_PACKAGES = "basePackages";
    /**
     * EnableElasticJob参数：扫描Elastic Job作业的基础类集合
     */
    String ENABLE_ELASTIC_JOB_ATTR_BASE_PACKAGE_CLASSES = "basePackageClasses";
    /**
     * EnableElasticJob参数：自动加载Elastic Job作业
     */
    String ENABLE_ELASTIC_JOB_ATTR_SMART_LOAD = "smartLoad";
    /**
     * EnableElasticJob参数： 扫描使用的注解
     */
    String ENABLE_ELASTIC_JOB_ATTR_LOAD_ANNOTATIONS = "loadAnnotation";
    /**
     * Monitor参数: 是否监控作业运行时状态
     */
    String MONITOR_ATTR_MONITOR_EXECUTION = "monitorExecution";
    /**
     * Monitor参数: 作业监控端口
     */
    String MONITOR_ATTR_MONITOR_PORT = "monitorPort";

    /**
     * *Elastic_job参数: 作业名称
     */
    String ELASTIC_JOB_COMMON_JOB_NAME = "jobName";
    /**
     * *Elastic_job参数: cron表达式
     */
    String ELASTIC_JOB_COMMON_CRON = "cron";
    /**
     * *Elastic_job参数: 作业分片总数
     */
    String ELASTIC_JOB_COMMON_SHARDING_TOTAL_COUNT = "shardingTotalCount";
    /**
     * *Elastic_job参数: 作业分片参数
     */
    String ELASTIC_JOB_COMMON_SHARDING_ITEM_PARAMETERS = "shardingItemParameters";
    /**
     * 作业自定义参数
     */
    String ELASTIC_JOB_COMMON_JOB_PARAMETER = "jobParameter";
    /**
     * *Elastic_job参数: 是否开启任务执行失效转移
     */
    String ELASTIC_JOB_COMMON_FAILOVER = "failover";
    /**
     * *Elastic_job参数: 是否开启错过任务重新执行
     */
    String ELASTIC_JOB_COMMON_MISFIRE = "misfire";
    /**
     * *Elastic_job参数: 作业描述信息
     */
    String ELASTIC_JOB_COMMON_DESCRIPTION = "description";

    Class<? extends CoordinatorRegistryCenter> DEFAULT_REGISTER_CENTER = ZookeeperRegistryCenter.class;


}
