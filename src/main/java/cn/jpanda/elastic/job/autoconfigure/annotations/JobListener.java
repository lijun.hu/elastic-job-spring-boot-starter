package cn.jpanda.elastic.job.autoconfigure.annotations;

import com.dangdang.ddframe.job.lite.api.listener.ElasticJobListener;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 全局作业监听器，该监听器将会在所有作业节点执行监听。
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/3 15:41
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface JobListener {
    /**
     * 前置后置任务监听实现类，需实现ElasticJobListener接口集合
     */
    Class<? extends ElasticJobListener>[] listenClass();

    long startedTimeoutMilliseconds() default -1L;

    long completedTimeoutMilliseconds() default -1L;


}
