package cn.jpanda.elastic.job.autoconfigure.properties;

import lombok.Data;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Zookeeper注册中心配置
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/18 13:53
 */
@Data
@ConfigurationProperties(prefix = "spring.elastic.job.zookeeper")
@ConditionalOnMissingBean
public class ZookeeperProperties {
    /**
     * 连接Zookeeper服务器的列表,包括IP地址和端口号,多个地址用逗号分隔。
     * 如: host1:2181,host2:2181
     */
    private String serverLists;

    /**
     * 命名空间.
     */
    private String namespace;

    /**
     * 等待重试的间隔时间的初始值.
     * 单位毫秒.
     */
    private Integer baseSleepTimeMilliseconds;

    /**
     * 等待重试的间隔时间的最大值.
     * 单位毫秒.
     */
    private Integer maxSleepTimeMilliseconds;

    /**
     * 最大重试次数.
     */
    private Integer maxRetries;

    /**
     * 会话超时时间.
     * 单位毫秒.
     */
    private Integer sessionTimeoutMilliseconds;

    /**
     * 连接超时时间.
     * 单位毫秒.
     */
    private Integer connectionTimeoutMilliseconds;

    /**
     * 连接Zookeeper的权限令牌.
     * 缺省为不需要权限验证.
     */
    private String digest;
}
