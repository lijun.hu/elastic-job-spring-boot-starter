package cn.jpanda.elastic.job.autoconfigure.strategy;

import cn.jpanda.elastic.job.autoconfigure.annotations.JobParser;
import cn.jpanda.elastic.job.autoconfigure.annotations.jobs.DataFlowElasticJob;
import com.dangdang.ddframe.job.config.script.ScriptJobConfiguration;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.context.annotation.ScannedGenericBeanDefinition;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

/**
 * 流式作业注解解析
 *
 * @author Hanqi <jpanda@aliyun.com>
 * @since 2018/9/25 17:44
 */
@JobParser(DataFlowElasticJob.class)
public class DataFlowJobParseAnnotationHandlerStrategy extends AbstractJobParseAnnotationHandlerStrategy {
    @Override
    protected AnnotationAttributes getJobCoreAnnotationAttrs(ScannedGenericBeanDefinition jobBeanDefinition, AnnotationMetadata metadata) {
        return AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(DataFlowElasticJob.class.getCanonicalName()));
    }

    @Override
    protected BeanDefinition getJobTypeConfigurationBeanDefinition(ScannedGenericBeanDefinition jobBeanDefinition, AnnotationMetadata metadata) {
        BeanDefinitionBuilder result = BeanDefinitionBuilder.rootBeanDefinition(ScriptJobConfiguration.class);
        result.addConstructorArgValue(getJobCoreConfiguration(jobBeanDefinition, metadata));
        result.addConstructorArgValue(jobBeanDefinition.getBeanClassName());

        AnnotationAttributes
                dataFlowJobAttributes = AnnotationAttributes.fromMap(metadata.getAnnotationAttributes(DataFlowElasticJob.class.getCanonicalName()));
        assert dataFlowJobAttributes != null;
        result.addConstructorArgValue(dataFlowJobAttributes.getString("streamingProcess"));
        return result.getBeanDefinition();
    }
}
