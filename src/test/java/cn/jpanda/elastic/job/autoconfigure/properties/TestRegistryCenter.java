package cn.jpanda.elastic.job.autoconfigure.properties;

import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;

import java.util.List;

public class TestRegistryCenter implements CoordinatorRegistryCenter {
    @Override
    public String getDirectly(String key) {
        return null;
    }

    @Override
    public List<String> getChildrenKeys(String key) {
        return null;
    }

    @Override
    public int getNumChildren(String key) {
        return 0;
    }

    @Override
    public void persistEphemeral(String key, String value) {

    }

    @Override
    public String persistSequential(String key, String value) {
        return null;
    }

    @Override
    public void persistEphemeralSequential(String key) {

    }

    @Override
    public void addCacheData(String cachePath) {

    }

    @Override
    public void evictCacheData(String cachePath) {

    }

    @Override
    public Object getRawCache(String cachePath) {
        return null;
    }

    @Override
    public void init() {

    }

    @Override
    public void close() {

    }

    @Override
    public String get(String key) {
        return null;
    }

    @Override
    public boolean isExisted(String key) {
        return false;
    }

    @Override
    public void persist(String key, String value) {

    }

    @Override
    public void update(String key, String value) {

    }

    @Override
    public void remove(String key) {

    }

    @Override
    public long getRegistryCenterTime(String key) {
        return 0;
    }

    @Override
    public Object getRawClient() {
        return null;
    }
}
