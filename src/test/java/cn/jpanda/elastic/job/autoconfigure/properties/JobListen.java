package cn.jpanda.elastic.job.autoconfigure.properties;

import com.dangdang.ddframe.job.executor.ShardingContexts;
import com.dangdang.ddframe.job.lite.api.listener.ElasticJobListener;

import java.util.Date;
public class JobListen implements ElasticJobListener {
    @Override
    public void beforeJobExecuted(ShardingContexts shardingContexts) {
        System.out.println("starter:"+new Date().getTime());
    }

    @Override
    public void afterJobExecuted(ShardingContexts shardingContexts) {
        System.out.println("end"+new Date().getTime());
    }
}
