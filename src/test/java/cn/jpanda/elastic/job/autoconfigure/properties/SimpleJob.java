package cn.jpanda.elastic.job.autoconfigure.properties;

import cn.jpanda.elastic.job.autoconfigure.annotations.JobEvent;
import cn.jpanda.elastic.job.autoconfigure.annotations.JobHandler;
import cn.jpanda.elastic.job.autoconfigure.annotations.JobListener;
import cn.jpanda.elastic.job.autoconfigure.annotations.JobMonitor;
import cn.jpanda.elastic.job.autoconfigure.annotations.jobs.SimpleElasticJob;
import com.dangdang.ddframe.job.api.ShardingContext;

@SimpleElasticJob(cron = "0/5 * * * * ?")
@JobListener(listenClass = {JobListen.class, JobOnceListener.class})
@JobHandler(exceptionHandlers = TestJobExceptionHandler.class)
@JobMonitor(monitorPort = 5027)
//@RegistryCenter(registerCenter = TestRegistryCenter.class)
@JobEvent
public class SimpleJob implements com.dangdang.ddframe.job.api.simple.SimpleJob {
    @Override
    public void execute(ShardingContext shardingContext) {
        System.out.println("A");
        throw new RuntimeException("aaa");
    }
}
