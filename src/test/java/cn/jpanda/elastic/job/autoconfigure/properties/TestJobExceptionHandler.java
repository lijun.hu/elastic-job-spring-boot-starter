package cn.jpanda.elastic.job.autoconfigure.properties;

import com.dangdang.ddframe.job.executor.handler.JobExceptionHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestJobExceptionHandler implements JobExceptionHandler {
    @Override
    public void handleException(String jobName, Throwable cause) {
        log.info("{}发生异常：{}",jobName,cause.getMessage());
    }
}
