package cn.jpanda.elastic.job;

import cn.jpanda.elastic.job.autoconfigure.annotations.EnableElasticJob;
import cn.jpanda.elastic.job.autoconfigure.properties.ZookeeperProperties;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;

import javax.annotation.Resource;
import javax.sql.DataSource;

@EnableElasticJob
@SpringBootApplication
public class TestStarter {
    public static void main(String[] args) {
        SpringApplication.run(TestStarter.class, args);
    }

    /**
     * Zookeeper属性配置
     */
    @Resource
    ZookeeperProperties zookeeperProperties;

//    @Resource
//    DataSourceProperties dataSourceProperties;

    /**
     * 配置Zookeeper注册中心
     *
     * @return Zookeeper注册中心
     */
    @Bean(initMethod = "init")
    public ZookeeperRegistryCenter zookeeperRegistryCenter() {
        ZookeeperConfiguration zookeeperConfiguration = new ZookeeperConfiguration(
                zookeeperProperties.getServerLists(),
                zookeeperProperties.getNamespace()
        );
        BeanUtils.copyProperties(zookeeperProperties, zookeeperConfiguration);
        return new ZookeeperRegistryCenter(zookeeperConfiguration);

    }

//    @Bean
//    public DataSource dataSource() {
//        MysqlDataSource dataSource = new MysqlDataSource();
//        dataSource.setURL(dataSourceProperties.getUrl());
//        dataSource.setUser(dataSourceProperties.getUsername());
//        dataSource.setPassword(dataSourceProperties.getPassword());
//        return dataSource;
//    }
}
