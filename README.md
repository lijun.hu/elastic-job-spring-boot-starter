# 整合 SPRING BOOT ELASTIC-JOB，编写自己的elastic-job-spring-boot-starter

> 依赖版本：

```
spring boot 全家桶版本：2.0.1.RELEASE

elastic-job版本:2.1.5

```

# 使用说明
## 启用Elastic-job
在spring boot 项目中可以在项目启动类上添加`@EnableElasticJob`注解来开启Elastic-job功能。

`@EnableElasticJob`有几个参数：
- enabled 参数表示是否开启Elastic-job功能，默认为`true`。
- basePackages 参数可以接收一个字符串数组，里面是想要扫描的ElasticJob作业实例所在的包名。
- basePackageClasses 参数接收一个Class数组，数组中的class对应的包名将会被添加到basePackages中。
- smartLoad参数本质上是用来禁用注解扫描功能，采用路径匹配的模式，默认为false，
暂时未实现对应的功能，对于任务配置依然需要对应的注解。
- loadAnnotation表示扫描的注解类，默认为`@ElasticJobMeteData`，无需修改。

## 配置Elstic job作业
- 在包`cn.jpanda.elastic.job.autoconfigure.annotations.jobs`下提供三个目前Elasticjob
支持的作业类型对应的注解。
- @SimpleElasticJob
- @ScriptElasticJob
- @DataFlowElasticJob
分别对应着简单作业，脚本作业和流式作业。
注解中含有的参数对应着ElasticJob作业的核心配置项。
- jobName 表示作业名称，如果为空，则使用作业实现类的全限定名称作为任务名称。
- cron 表示cron表达式，用于控制任务触发时间
- shardingTotalCount 表示作业分片总数
- shardingItemParameters 表示分片参数
- jobParameter 表示作业自定义参数
- failover 表示是否开启任务执行失效转移，默认为false
- misfire 表示是否开启错过任务重新执行
- description 表示作业描述信息
- jobShardingStrategyClass 表示作业分片策略实现类的全限定名称
- reconcileIntervalMinutes 表示修复作业服务器不一致状态服务调度间隔时间
- maxTimeDiffSeconds 表示最大允许的本机与注册中心的时间误差秒数
- disabled
- overwrite

除此之外，对于脚本型作业和流式作业还有特有的配置项：

 `@ScriptElasticJob`
- scriptCommandLine biaoshi 脚本型作业执行命令行

`@DataFlowElasticJob`
- streamingProcess 表示是否流式处理数据

> 对于一个简单的作业来说，只需要使用上述的`@***ElasticJob`注解来进行配置就可以了。

## 事件追踪
如果需要开启作业的事件追踪功能，可以使用`@JobEvent`注解，`@JobEvent`注解有两个参数。
- enabled 表示是否开启事件追踪功能，默认为true,
- eventHandlerClass 表示处理事件追踪的处理类，默认为JobEventRdbConfiguration类。
所以如果开启了事件追踪功能，往往也就需要配置一个JobEventRdbConfiguration实例。

## 定制作业线程处理器和异常处理器
如果需要定制作业的线程处理器和异常处理器可以使用`@JobHandler`注解来设定对应的值。
`@JobHandler`注解有两个参数：
- executorServiceHandlers表示线程处理器，默认为DefaultExecutorServiceHandler。
- exceptionHandlers 表示异常处理器，默认为DefaultJobExceptionHandler。

## 开启作业监听器
如果需要在业务中开启事件监听器可以使用`@JobListener`注解，`@JobListener`注解有三个参数：
- listenClass表示监听器集合，需实现ElasticJobListener接口集合。
- startedTimeoutMilliseconds 表示开始超时时间
- completedTimeoutMilliseconds 表示结束超时时间

对于普通的ElasticJobListener实现来说可以不用理会startedTimeoutMilliseconds和completedTimeoutMilliseconds参数。

对于AbstractDistributeOnceElasticJobListener来说，则需要配置这两个参数，

> 框架会根据监听器实现的接口来判断监听器的类型。

## 开启作业的监控配置

如果需要开启作业的监控配置，可以使用注解`@JobMonitor`来实现：
- monitorExecution 表示监控作业运行时状态
- monitorPort 表示作业监听端口

## 指定作业对应的注册中心

如果需要指定作业使用的注册中心，可以使用注解`@RegistryCenter`来实现。
- registerCenter 表示作业使用的注册中心，默认为ZookeeperRegistryCenter。

## 扩展

目前Elastic Job只有script，simple和dataflow三种类型的作业。
如果需要扩展新类型的任务，或者定制任务注解的解析，可以新增一个注解类，并使用`@ElasticJobMeteData`注解来标注这个新注解。
同时，新增一个类实现`JobParseAnnotationHandlerStrategy`接口的方法，或者继承`AbstractJobParseAnnotationHandlerStrategy`,并根据自己的需要重写或实现相应的方法。并在实现类上添加注解`@JobParser`.
同时将`@JobParser`的value设为新增的类型。



> 涉及文档
- [ElasticJob官方文档地址](http://elasticjob.io/docs/elastic-job-lite/00-overview/)

## 1.通过官方文档和JobSchedule类，归纳所需要的公共配置.
> 从JobScheduler类入手,辅以官方文档，归纳定义任务时所需要的所有配置。

> [elasticjob文档](http://elasticjob.io/docs/elastic-job-lite/02-guide/config-manual/)。

首先从JobScheduler类入手，找到最复杂的构造方法，该构造方法涉及到的参数为:

- CoordinatorRegistryCenter(用于协调分布式服务的注册中心)
- LiteJobConfiguration(Lite作业配置)          
- JobEventBus(运行痕迹时间总线)
- ElasticJobListener(弹性化分布式作业监听器)

### CoordinatorRegisterCenter

CoordinatorRegistryCenter目前实现类是ZookeeperRegistryCenter，其中包含的配置为ZookeeperConfiguration,对于注册中心来讲通常是全局唯一的,所以我们可能需要一个Zookeeper注册中心配置类(ZookeeperProperties),这个配置类应该包含了ZookeeperConfiguration中的所有可定制的参数。
![image](0569981B65C3403AB0FE12258B4BA48A)

### LiteJobConfiguration
LiteJobConfiguration用来配置具体的Lite作业，这里面主要涉及了三个类。
- JobCoreConfiguration
- JobTypeConfiguration
- LiteJobConfiguration

![image](7B91E4D6CCBC49098AE4C91659773A3A)

JobCoreConfiguration是一个作业的核心配置，他被JobTypeConfiguration引用，来填充作业的基础的、核心的配置内容。

JobTypeConfiguration在持有了JobCoreConfiguration的同时，还包含了针对不同类型的作业的配置，可用于进一步划分不同的作业。

最后，LiteJobConfiguration集成了JobTypeConfiguration的内容，并提供了额外的配置，包括监控，分片策略等内容

#### JobCoreConfiguration

因为在Elastic-job中，JobCoreConfiguration作为最底层的构造单元之一，将作用在每一个作业上，在不需要特殊定制JobCoreConfiguration的场景中，定义一个全局的JobCoreConfiguration可以大大减少工作量，所以JobCoreConfiguration可以考虑成为一个全局的默认配置项出现。

同时，在具体的作业中，不同的作业，可能会对JobCoreConfiguration进行一些定制化的操作，这也要要求JobCoreConfiguration能够单独配置。

因此，对于JobCoreConfiguration在提供全局配置的同时，还要提供定制性的配置方案，因此可以考虑默认全局配置，然后定制配置覆盖默认配置的方式来实现。
- ![image](C0FB2A3E2C2841B2BA565A8BCBDFD342)

#### JobTypeConfiguration

之后，对于稍上一层的JobTypeConfiguration来说，他的作用除了持有JobCoreconfiguration之外，就是对作业类型的分装和作业实现类的封装。
在JobTypeConfiguration中将任务划分为三种:

- SimpleJob(简单任务)
- DataflowJob(流式任务)
- ScriptJob(脚本任务)
![image](68577245A52C49798D408C0F21FFB8A5)

三种不同的任务类型，也对应着其各自不同的、特有的任务定义。
  - ![image](EB71943E8CE5452784C40B79E93FF408)

鉴于此，对于JobTypeConfiguration来讲，需要针对这三种任务来进行其单独的配置，因为这一配置需要和具体的作业相关联，所以考虑考虑将其作为注解的形式来实现。

#### LiteJobConfiguration
最后，在JobCoreConfiguration和JobTypeConfiguration配置完成之后，就又回到了LiteJobConfiguration中，在LiteJobConfiguration中，除了上述的两类配置之外，还提供了关于监控、分片策略的配置。
![image](257E035EE51846B8956D0E50462DF224)

到这为止，关于作业配置的方面，总结一下我们需要：

- JobCoreConfiguration全局配置方案
- JobCoreConfiguration局部定制配置方案
- SimpleJob，DataFlowJob，ScriptJob独立的配置方案。
- 监控/分片策略配置方案.


> 关于具体的配置属性，可以在com.dangdang:elastic-job-lite-spring:2.1.5中的META-INF/namespace中的job.xsd和reg.xsd文件中查看。

### JobEventBus

在分析了LiteJobConfiguration之后，继续拆分JobSheduler的构造参数——JobEventBus。

JobEventBus提供了Elastic-job的事件追踪功能，他主要引用了一个叫做JobEventConfiguration的作业事件配置接口，目前该接口的实现可以算是只有JobEventRdbConfiguration类，在JobEventRdbConfiguration类中引用了DataSource数据源，仅此而已。

鉴于此，我们可以考虑采用项目默认的数据源，但是也要考虑采用独有的数据源。
因此，也要尽可能的为DataSource提供定制的支持。

### ElasticJobListener

最后，只剩下ElasticJobListener参数，ElasticJobListener是弹性化分布式作业监听器接口定义，目前在Elastic-job中提供了两种监听器的配置：
  - 分布式listener配置(全局)
  - 单节点listener配置(唯一)
对于普通的分布式作业监听器来说，只要实现了ElasticJobListener即可,但是对于在分布式作业中只执行一次的监听器,也就是单节点监听配置,则要求集成AbstractDistributeOnceElasticJobListener类.

这两种监听器在配置上来看,主要是构造参数的不同,因此可以考虑定义为两个不同的注解.

## 2.拆分配置，分为注解和配置文件
截止到这,对于Elastic-job的配置部分已经整理的差不多了，接下来就是对上述涉及到的配置进行汇总分类。
### 全局配置
首先是全局配置，对于Zookeeper注册中心的配置毋庸置疑是需要提供全局配置的。

之后是JobCoreConfiguration，在考虑到不需要为每个作业都单独定制的场景时，其也需要提供全局配置的方案，但是现实很骨感，对于JobCoreConfiguration来说，能复用的也就只有JobProperties参数，其他的均和具体的作业有强关联。

而LiteJobConfiguration中的监控和分片策略大概率下复用的可能性比较高，所以需要提供默认的实现。

最后是JobEventBus和ElasticJobListener，事件追踪和任务监听，可以独立存在，也可以不存在，其必要性相对比较弱，可以考虑不提供全局默认配置。
不过，需要考虑为JobEventBus提供DataSource的全局配置以及定义方案。

因此，以下部分需要提供全局配置方案：

- ZookeeperProperties(zookeeper配置中心)
- DataSourceProperties(JobEventBus的Datasource)
- MonitorProperites(LiteJobConfiguration的监控配置)
- ShardingStrategyProperties(LiteJobConfiguration的分片策略配置)
### 单独配置

关于单独配置，首先JobTypeConfiguration对应的三类任务，必然是需要提供单独配置的，
同时事件追中，以及监听也都需要提供单独的配置，而JobCoreConfiguration同样要为定制性的场景考虑单独配置。

针对独有的配置SimpleJob,DataflowJob,ScriptJob三个配置，他和具体的job一一对应，可以考虑作为注解来存在。
- @SimpleJob(简单任务配置)
- @DataflowJob(流式任务配置)
- @ScriptJob(脚本任务配置)

对于JobCoreConfiguration来说，可以提供单独的注解，不配置则采用默认注解，在配置的场景下则采用对应的配置。
- @JobCore - 作业核心配置

最后再针对任务监控，分片策略，事件追踪分别提供定制性的注解
- @JobMonitor(任务监控)
- @JobShardingStrategy(分片策略)
- @JobEvent(事件追踪)

# 和spring cloud整合

>在spring cloud Finchley*版本中引用了curator4.0+版本的jar包，但是在elastic-job使用的是2.10.0版本的jar包，两者不兼容，所以整合时需要重新定义一下curator系列的版本jar包。

```
    <com.dodomall.elastic-job-spring-boot-starter>1.0-SNAPSHOT</com.dodomall.elastic-job-spring-boot-starter>
    <org.apache.curator.curator.client>2.10.0</org.apache.curator.curator.client>
    <org.apache.curator.curator.framework>2.10.0</org.apache.curator.curator.framework>
    <org.apache.curator.curator.recipes>2.10.0</org.apache.curator.curator.recipes>
```
```
<dependencyManagement>
    <dependencies>
        <dependency>
                <groupId>org.apache.curator</groupId>
                <artifactId>curator-client</artifactId>
                <version>${org.apache.curator.curator.client}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.curator</groupId>
                <artifactId>curator-framework</artifactId>
                <version>${org.apache.curator.curator.framework}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.curator</groupId>
                <artifactId>curator-recipes</artifactId>
                <version>${org.apache.curator.curator.recipes}</version>
            </dependency>
        </dependencies>
</dependencyManagement>
```